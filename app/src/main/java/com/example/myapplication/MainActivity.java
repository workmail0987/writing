package com.example.myapplication;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {
    SharedPreferences.Editor editor;
    SharedPreferences preferences;
    EditText edtContentBox;
    FloatingActionButton floatingActionButton, btn_floatingAction_summary,
            btn_floatingAction_summaryTitle;
    float distanceX;
    int lastAction;
    private float dX, dY;
    private float downRawX, downRawY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtContentBox = findViewById(R.id.txt_letter);
        preferences = getSharedPreferences("writingContent", MODE_PRIVATE);
        floatingActionButton = findViewById(R.id.btn_floatingAction);
        btn_floatingAction_summary = findViewById(R.id.btn_floatingAction_summary);
        btn_floatingAction_summaryTitle = findViewById(R.id.btn_floatingAction_summaryTitle);
        editor = getSharedPreferences("writingContent", MODE_PRIVATE).edit();
        if (preferences.contains("content")) {
            edtContentBox.setText(preferences.getString("content", "EmptyPreferences"));
            edtContentBox.setSelection(edtContentBox.getText().length());
        }
        floatingActionButton.setOnTouchListener(this::onTouch);
        btn_floatingAction_summary.setOnTouchListener(this::onTouch);
        btn_floatingAction_summaryTitle.setOnTouchListener(this::onTouch);

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("TAG", "on pause called and data is saved");
        editor.putString("content", edtContentBox.getText().toString());
        editor.commit();
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();

        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:

                downRawX = event.getRawX();
                downRawY = event.getRawY();
                dX = view.getX() - downRawX;
                dY = view.getY() - downRawY;

                lastAction = MotionEvent.ACTION_DOWN;
                break;

            case MotionEvent.ACTION_MOVE:
                int viewWidth = view.getWidth();
                int viewHeight = view.getHeight();

                View viewParent = (View) view.getParent();
                int parentWidth = viewParent.getWidth();
                int parentHeight = viewParent.getHeight();

                float newX = event.getRawX() + dX;
                newX = Math.max(layoutParams.leftMargin, newX); // Don't allow the FAB past the left hand side of the parent
                newX = Math.min(parentWidth - viewWidth - layoutParams.rightMargin, newX); // Don't allow the FAB past the right hand side of the parent

                float newY = event.getRawY() + dY;
                newY = Math.max(layoutParams.topMargin, newY); // Don't allow the FAB past the top of the parent
                newY = Math.min(parentHeight - viewHeight - layoutParams.bottomMargin, newY); // Don't allow the FAB past the bottom of the parent

                view.animate()
                        .x(newX)
                        .y(newY)
                        .setDuration(0)
                        .start();
                lastAction = MotionEvent.ACTION_MOVE;
                break;

            case MotionEvent.ACTION_UP:
                distanceX = event.getRawX() - downRawX;
                if (Math.abs(distanceX) < 10) {

                    if (view.getId() == R.id.btn_floatingAction) {
                        edtContentBox.append(" \uD83E\uDEB6 ");
                        editor.putString("content", edtContentBox.getText().toString());
                        editor.commit();
                        Toast.makeText(MainActivity.this, "Saved Successfully", Toast.LENGTH_SHORT).show();

                    } else if (view.getId() == R.id.btn_floatingAction_summary) {
                        showSummaryDialog();
                        //Toast.makeText(MainActivity.this, "Summary button clicked", Toast.LENGTH_SHORT).show();
                    } else if (view.getId() == R.id.btn_floatingAction_summaryTitle) {
                        showSummaryDialogWithTitle();
                    }

                }

                break;

            case MotionEvent.ACTION_BUTTON_PRESS:


            default:
                return false;
        }
        return true;
    }

    public void showSummaryDialogWithTitle() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.summary_dialog_view, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.create();
        AlertDialog dialog = dialogBuilder.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        EditText editText = (EditText) dialog.findViewById(R.id.txt_letter_summary);
        EditText editText_summary = (EditText) dialog.findViewById(R.id.txt_letter_summary_title);

        editText_summary.setVisibility(View.VISIBLE);

        if (preferences.contains("summary") || preferences.contains("summaryTitle")) {
            editText.setText(preferences.getString("summary", ""));
            editText_summary.setText(preferences.getString("summaryTitle", ""));
            editText.setSelection(editText.getText().length());
            editText_summary.setSelection(editText_summary.getText().length());
        }

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                editor.putString("summary", editText.getText().toString());
                editor.putString("summaryTitle", editText_summary.getText().toString());
                editor.commit();
            }
        });
    }

    public void showSummaryDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.summary_dialog_view, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.create();
        AlertDialog dialog = dialogBuilder.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        EditText editText = (EditText) dialog.findViewById(R.id.txt_letter_summary);
        EditText editText_summary = (EditText) dialog.findViewById(R.id.txt_letter_summary_title);
        editText_summary.setVisibility(View.INVISIBLE);

        if (preferences.contains("summary")) {
            editText.setText(preferences.getString("summary", ""));
            editText.setSelection(editText.getText().length());
        }

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                editor.putString("summary", editText.getText().toString());
                editor.commit();
            }
        });


    }

}